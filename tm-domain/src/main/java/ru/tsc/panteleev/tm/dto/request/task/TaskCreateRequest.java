package ru.tsc.panteleev.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class TaskCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    public TaskCreateRequest(@Nullable String token,
                             @Nullable String name,
                             @Nullable String description,
                             @Nullable Date dateBegin,
                             @Nullable Date dateEnd
    ) {
        super(token);
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

}

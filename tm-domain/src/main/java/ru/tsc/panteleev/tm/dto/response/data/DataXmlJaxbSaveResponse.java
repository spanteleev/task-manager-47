package ru.tsc.panteleev.tm.dto.response.data;

import lombok.NoArgsConstructor;
import ru.tsc.panteleev.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public class DataXmlJaxbSaveResponse extends AbstractResponse {
}

package ru.tsc.panteleev.tm.exception.field;

public final class RoleIncorrectException extends AbstractFieldException {

    public RoleIncorrectException() {
        super("Error! Role is incorrect...");
    }

}

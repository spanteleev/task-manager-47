package ru.tsc.panteleev.tm.dto.response.user;


import lombok.NoArgsConstructor;
import ru.tsc.panteleev.tm.dto.model.UserDTO;

@NoArgsConstructor
public class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(UserDTO user) {
        super(user);
    }

}

package ru.tsc.panteleev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.task.*;
import ru.tsc.panteleev.tm.dto.response.task.*;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ITaskEndpoint extends IEndpoint {

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, NAMESPACE, PART, ITaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, NAMESPACE, PART, ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    @WebMethod
    TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull
    @WebMethod
    TaskCompleteByIdResponse completeTaskStatusById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull
    @WebMethod
    TaskListResponse listTask(@NotNull TaskListRequest request);

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    @WebMethod
    TaskGetByIdResponse showTaskById(@NotNull TaskGetByIdRequest request);

    @NotNull
    @WebMethod
    TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request);

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    @WebMethod
    TaskShowListByProjectIdResponse showListByProjectIdTask(@NotNull TaskGetListByProjectIdRequest request);

}

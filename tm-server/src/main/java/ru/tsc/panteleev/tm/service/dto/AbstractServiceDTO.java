package ru.tsc.panteleev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.panteleev.tm.api.service.IConnectionService;
import ru.tsc.panteleev.tm.api.service.dto.IServiceDTO;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDTO;

public abstract class AbstractServiceDTO<M extends AbstractModelDTO, R extends IRepositoryDTO<M>> implements IServiceDTO<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractServiceDTO(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}

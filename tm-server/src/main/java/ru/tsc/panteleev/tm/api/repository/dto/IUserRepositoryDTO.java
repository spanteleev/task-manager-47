package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.model.UserDTO;
import java.util.Collection;
import java.util.List;

public interface IUserRepositoryDTO extends IRepositoryDTO<UserDTO> {

    void set(@NotNull Collection<UserDTO> users);

    List<UserDTO> findAll();

    UserDTO findById(@NotNull String id);

    void removeById(@NotNull String id);

    void clear();

    long getSize();

    UserDTO findByLogin(@NotNull String login);

    UserDTO findByEmail(@NotNull String email);

}

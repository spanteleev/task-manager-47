package ru.tsc.panteleev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.model.Project;
import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable String userId,
                   @Nullable String name,
                   @Nullable String description,
                   @Nullable Date dateBegin,
                   @Nullable Date dateEnd
    );

    @NotNull
    Project updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

}

package ru.tsc.panteleev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDTO;
import ru.tsc.panteleev.tm.enumerated.Sort;

import javax.persistence.EntityManager;

public abstract class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRepositoryDTO(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected String getSortColumn(@Nullable final Sort sort) {
        return sort.getOrderColumn();
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}

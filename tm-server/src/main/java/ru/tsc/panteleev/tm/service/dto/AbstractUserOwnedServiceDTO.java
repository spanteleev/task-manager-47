package ru.tsc.panteleev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.panteleev.tm.api.service.IConnectionService;
import ru.tsc.panteleev.tm.api.service.dto.IUserOwnedServiceDTO;
import ru.tsc.panteleev.tm.dto.model.AbstractUserOwnedModelDTO;

public abstract class AbstractUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedRepositoryDTO<M>>
        extends AbstractServiceDTO<M, R> implements IUserOwnedServiceDTO<M> {

    public AbstractUserOwnedServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}

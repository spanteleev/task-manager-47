package ru.tsc.panteleev.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "789624";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "147952368";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    public static final String SERVER_PORT_KEY_DEFAULT = "8080";

    @NotNull
    public static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    public static final String SERVER_HOST_KEY_DEFAULT = "localhost";

    @NotNull
    public static final String SESSION_KEY = "session.key";

    @NotNull
    public static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    private static final String DATABASE_USER_NAME_KEY = "database.username";

    @NotNull
    private static final String DATABASE_USER_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    private static final String DATABASE_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DATABASE_SQL_DIALECT = "database.sql_dialect";

    @NotNull
    private static final String DATABASE_HBM_2_DDL_AUTO = "database.hbm2dll_auto";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_CACHE_USE_SECOND_LEVEL_CACHE = "database.cache.use_second_level_cache";

    @NotNull
    private static final String DATABASE_CACHE_PROVIDER_CONFIG_FILE_RESOURCE_PATH = "database.cache.provider_configuration_file_resource_path";

    @NotNull
    private static final String DATABASE_CACHE_REGION_FACTORY_CLASS = "database.cache.region.factory_class";

    @NotNull
    private static final String DATABASE_CACHE_USE_QUERY_CACHE = "database.cache.use_query_cache";

    @NotNull
    private static final String DATABASE_CACHE_USE_MINIMAL_PUTS = "database.cache.use_minimal_puts";

    @NotNull
    private static final String DATABASE_CACHE_REGION_PREFIX = "database.cache.region_prefix";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key))
            return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey))
            return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_KEY_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getServerHost() {
        @NotNull final String value = getStringValue(SERVER_HOST_KEY, SERVER_HOST_KEY_DEFAULT);
        return value;
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull final String value = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return Integer.parseInt(getStringValue(SESSION_TIMEOUT));
    }

    @NotNull
    @Override
    public String getDatabaseUserName() {
        return getStringValue(DATABASE_USER_NAME_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseUserPassword() {
        return getStringValue(DATABASE_USER_PASSWORD_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DATABASE_URL_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DATABASE_SQL_DIALECT);
    }

    @NotNull
    @Override
    public String getDatabaseHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM_2_DDL_AUTO);
    }

    @NotNull
    @Override
    public String getDatabaseShowSql() {
        return getStringValue(DATABASE_SHOW_SQL);
    }

    @Override
    public @NotNull String getDatabaseUseSecondLvlCache() {
        return getStringValue(DATABASE_CACHE_USE_SECOND_LEVEL_CACHE);
    }

    @NotNull
    @Override
    public String getDatabaseCacheConfigFile() {
        return getStringValue(DATABASE_CACHE_PROVIDER_CONFIG_FILE_RESOURCE_PATH);
    }

    @NotNull
    @Override
    public String getDatabaseCacheFactoryClass() {
        return getStringValue(DATABASE_CACHE_REGION_FACTORY_CLASS);
    }

    @NotNull
    @Override
    public String getDatabaseUseQueryCache() {
        return getStringValue(DATABASE_CACHE_USE_QUERY_CACHE);
    }

    @NotNull
    @Override
    public String getDatabaseUseMinimalPuts() {
        return getStringValue(DATABASE_CACHE_USE_MINIMAL_PUTS);
    }

    @NotNull
    @Override
    public String getDatabaseCacheRegionPrefix() {
        return getStringValue(DATABASE_CACHE_REGION_PREFIX);
    }
}

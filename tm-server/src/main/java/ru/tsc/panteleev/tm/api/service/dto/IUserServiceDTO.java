package ru.tsc.panteleev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.dto.model.UserDTO;

public interface IUserServiceDTO extends IServiceDTO<UserDTO> {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findById(@NotNull String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    void remove(@Nullable final UserDTO user);

    void removeByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @Nullable
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @Nullable
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}

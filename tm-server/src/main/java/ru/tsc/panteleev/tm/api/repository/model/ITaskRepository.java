package ru.tsc.panteleev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.model.Task;
import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    void set(@NotNull Collection<Task> tasks);

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<Task> findAll();

    @Nullable
    Task findById(@NotNull String userId, @NotNull String id);

    void removeById(@NotNull String userId, @NotNull String id);

    void clearByUserId(@NotNull String userId);

    void clear();

    long getSize(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId);

}

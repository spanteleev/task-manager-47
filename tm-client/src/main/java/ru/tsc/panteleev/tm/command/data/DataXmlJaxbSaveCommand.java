package ru.tsc.panteleev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.data.DataXmlJaxbSaveRequest;

public class DataXmlJaxbSaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save data in xml file";

    @NotNull
    private static final String NAME = "data-save-xml-jaxb";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        showDescription();
        getDomainEndpoint().saveDataXmlJaxb(new DataXmlJaxbSaveRequest(getToken()));
    }

}

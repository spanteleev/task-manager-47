package ru.tsc.panteleev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-unbind-from-project";

    @NotNull
    public static final String DESCRIPTION = "Task unbind from project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getProjectTaskEndpoint().unbindFromProjectTaskById(new TaskUnbindFromProjectRequest(getToken(), projectId, taskId));
    }

}

package ru.tsc.panteleev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.task.TaskClearRequest;

public class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        getTaskEndpoint().clearTask(new TaskClearRequest(getToken()));
    }

}

package ru.tsc.panteleev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.data.DataBackupSaveRequest;

public class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save backup";

    @NotNull
    public static final String NAME = "data-save-backup";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        showDescription();
        getDomainEndpoint().saveDataBackup(new DataBackupSaveRequest(getToken()));
    }

}
